-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 13, 2018 at 05:55 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `market`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `ostan` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `shahr` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `ename` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `parent_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `ename`, `parent_id`) VALUES
(8, 'آموزش اندروید', 'android', 0),
(9, 'آموزش php', 'php', 0),
(10, 'آموزش لاراول', 'laravel', 9),
(11, 'آموزشhtml', 'html', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cat_product`
--

DROP TABLE IF EXISTS `cat_product`;
CREATE TABLE IF NOT EXISTS `cat_product` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `product_id` int(255) NOT NULL,
  `cat_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `cat_product`
--

INSERT INTO `cat_product` (`id`, `product_id`, `cat_id`) VALUES
(20, 7, 9),
(21, 7, 10),
(42, 9, 9),
(43, 9, 10),
(44, 8, 8),
(45, 5, 8),
(46, 5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `comment` text COLLATE utf8_persian_ci NOT NULL,
  `product_id` int(225) NOT NULL,
  `state` int(1) NOT NULL,
  `time` int(10) NOT NULL,
  `parent_id` int(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `name`, `email`, `comment`, `product_id`, `state`, `time`, `parent_id`) VALUES
(79, 'سعید', 's4rft@yahoo.com', 'خوب بود', 5, 1, 1534138670, 0),
(83, 'Mehran', 'softpedia.tec@gmail.com', 'تشکر', 6, 1, 1534138670, 0),
(88, 'سعید', 's4rft@yahoo.com', 'مرسی', 5, 1, 1534118710, 0),
(100, 'ali', 'ali.soleimani94@yahoo.com', 'عالی', 5, 1, 1534138670, 0),
(102, 'امین کریمی', 'amin66.karimi@gmail.com', 'فایل خراب بود', 5, 1, 1534138670, 0),
(108, 'علی', 'aliac9488@gmail.com', 'سلام', 5, 1, 1534138670, 0),
(112, 'مهدی', 'hamidjabarpoor66@gmail.com', 'تست', 5, 1, 1534138670, 0),
(146, 'Mehran', 'softpedia.tec@gmail.com', 'تست ', 8, 1, 1534138670, 0),
(149, 'علی ', 'aliasgharnasiri72@yahoo.com', 'خوب بود', 8, 1, 1534138670, 0),
(152, 'hossein', 'h.tadayyon@yahoo.com', 'با سلام و خسته نباشید.قسمت دو دانلود نمیشه', 8, 1, 1534138670, 0),
(157, 'nima rezaee', 'nimanima35@gmail.com', 'درخواست تخفیف', 5, 1, 1534138670, 0),
(246, 'محمد', 'madnasoft@yahoo.com', 'لاراول', 5, 1, 1534138670, 0);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `discounts_name` varchar(225) COLLATE utf8_persian_ci NOT NULL,
  `discounts_value` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `discounts_name`, `discounts_value`) VALUES
(6, 'php', 40);

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

DROP TABLE IF EXISTS `download`;
CREATE TABLE IF NOT EXISTS `download` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `file` varchar(1000) COLLATE utf8_persian_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `download`
--

INSERT INTO `download` (`id`, `file`, `name`, `time`) VALUES
(1, 'file/1.zip', '0d07fa6db09f33b0df05c14f5acdad05', 1468962790),
(2, 'file/2.zip', '0d07fa6db09f33b0df05c14f5acdad05', 1468962790),
(3, 'file/1.zip', 'cd58bdaaa8d4e61e372aa8f2fd88638d', 1468962907),
(4, 'file/2.zip', 'cd58bdaaa8d4e61e372aa8f2fd88638d', 1468962907),
(5, 'file/1.zip', '893e0100fd902cfcc32bc3aa0c4d3d9d', 1468963019),
(6, 'file/2.zip', '893e0100fd902cfcc32bc3aa0c4d3d9d', 1468963019),
(7, 'file/1.zip', 'e6292ce73786dbd4492b437e521ae8ce', 1468963125),
(8, 'file/2.zip', 'e6292ce73786dbd4492b437e521ae8ce', 1468963125),
(9, 'file/1.zip', '9be7d718686adbdb2211eeee3a209c60', 1468963140),
(10, 'file/2.zip', '9be7d718686adbdb2211eeee3a209c60', 1468963140),
(11, 'file/1.zip', '64413c7ecbe501a4d2e7ee6b7ab2fb11', 1468963152),
(12, 'file/2.zip', '64413c7ecbe501a4d2e7ee6b7ab2fb11', 1468963152),
(13, 'file/1.zip', '095f24294bc54239f0f813541b36a67b', 1468963269),
(14, 'file/2.zip', '095f24294bc54239f0f813541b36a67b', 1468963269),
(15, 'file/1.zip', '797820937ce1de9df56b7c293f9887ec', 1468971673),
(16, 'file/2.zip', '797820937ce1de9df56b7c293f9887ec', 1468971673),
(17, 'file/1.zip', '117215203868250c99fcab2d2eb523db', 1468971758),
(18, 'file/2.zip', '117215203868250c99fcab2d2eb523db', 1468971758),
(19, 'file/1.zip', 'c453a9f0cdc6b155286c43417e1b763b', 1468971780),
(20, 'file/2.zip', 'c453a9f0cdc6b155286c43417e1b763b', 1468971780),
(21, 'file/1.zip', 'be2947590905d1f1d45625659517a0b6', 1468971853),
(22, 'file/2.zip', 'be2947590905d1f1d45625659517a0b6', 1468971853),
(23, 'file/1.zip', '519b13af427c1a2fe58ea98a4a9e9f2b', 1468971883),
(24, 'file/2.zip', '519b13af427c1a2fe58ea98a4a9e9f2b', 1468971883),
(25, 'file/1.zip', 'e634aa00928021c97d53cbe135625965', 1468971911),
(26, 'file/2.zip', 'e634aa00928021c97d53cbe135625965', 1468971911);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_20_222732_create_session_table', 2),
('2016_07_20_223859_create_test_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `mobile` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `time` int(10) NOT NULL,
  `date` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `product_id` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `payment_status` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `RefId` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `saleReferenceId` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `zip_code` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `address` text COLLATE utf8_persian_ci NOT NULL,
  `order_read` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `total_price` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `price` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `fname`, `lname`, `email`, `mobile`, `time`, `date`, `product_id`, `payment_status`, `RefId`, `saleReferenceId`, `zip_code`, `address`, `order_read`, `total_price`, `price`) VALUES
(2, 'علی', 'علوی', 'ali.alavi@gmail.com', '09141592093', 1468799160, '1395-4-22', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(3, 'علی', 'علوی', 'ali.alavi@gmail.com', '09141592093', 1468799160, '1395-4-22', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(4, 'علی', 'علوی', 'ali.alavi@gmail.com', '09141592093', 1468799160, '1395-4-23', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(5, 'علی', 'علوی', 'ali.alavi@gmail.com', '09141592093', 1468799160, '1395-4-24', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(6, 'امیر', 'لشگری', 'amir.lashgari@gmail.com', '09141592093', 1468799160, '1395-4-24', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 25000),
(7, 'امیر', 'لشگری', 'amir.lashgari@gmail.com', '09141592093', 1468799160, '1395-4-25', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(8, 'امیر', 'لشگری', 'amir.lashgari@gmail.com', '09141592093', 1468799160, '1395-4-26', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(9, 'امیر', 'لشگری', 'amir.lashgari@gmail.com', '09141592093', 1468799160, '1395-4-27', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(10, 'امیر', 'لشگری', 'amir.lashgari@gmail.com', '09141592093', 1468799160, '1395-4-27', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(11, 'پوریا', 'صدیقی', 'poor.sedighi@gmail.com', '09141592093', 1468799160, '1395-4-28', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(12, 'پوریا', 'صدیقی', 'poor.sedighi', '09141592093', 1468799160, '1395-4-29', '5,8,', 'معلق', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '1500000', 15000),
(13, 'پوریا', 'صدیقی', 'poor.sedighi', '09141592093', 1468799160, '1395-4-29', '5,8,', 'پرداخت شده', '0318C1D5A29AEB7A', '', '1234567891', 'تهران', 'ok', '200000', 16000);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('araz@gmail.com', '1a23dad8251c3862a3fb62f8c5df94e5455a88ff191bd1de0e3dd28dc6caa34b', '2016-07-16 16:43:14'),
('ali.sedighi.tu@gmail.com', '07faa056491dfe122679cf8fef7478ead74b4250eb533acbbf94f5fd98c703fd', '2016-07-16 17:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) COLLATE utf8_persian_ci NOT NULL,
  `url` varchar(1000) COLLATE utf8_persian_ci NOT NULL,
  `content` text COLLATE utf8_persian_ci NOT NULL,
  `date` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `img` varchar(1000) COLLATE utf8_persian_ci NOT NULL,
  `price` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `order_number` int(10) NOT NULL,
  `download` text COLLATE utf8_persian_ci NOT NULL,
  `files_size` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `files_time` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `number_files` int(225) NOT NULL,
  `tag` text COLLATE utf8_persian_ci NOT NULL,
  `View` int(225) NOT NULL,
  `state` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `url`, `content`, `date`, `img`, `price`, `order_number`, `download`, `files_size`, `files_time`, `number_files`, `tag`, `View`, `state`) VALUES
(5, 'پیاده سازی فروشگاه اینترنتی مشابه دیجی کالا با PHP MVC', 'پیاده-سازی-فروشگاه-اینترنتی-مشابه-دیجی-کالا-با-PHP-MVC', '<p>به نام خدا</p>\r\n\r\n<p>با عرض سلام و ادب خدمت کاربران</p>\r\n\r\n<p>معماری <span style=\"color:#FF0000\">mvc</span> و ساخت پروژه های کابردی با این معماری از جمله مواردی هست که کمتر در منابع آموزشی مختلف&nbsp; به آن پرداخته شده لذا بر آن شدیم تا یک دوره کامل برای آموزش این معماری آماده کنیم</p>\r\n<!--more-->\r\n\r\n<div class=\"ck_div1\">\r\n<p><span style=\"color:#FF0000\">اهداف دوره آموزشی</span></p>\r\n\r\n<p>آموزش معماری mvc و مزیت استفاده از این معماری در ساخت پروژه ها</p>\r\n\r\n<p>آموزش نحوه به کارگیری شی گرایی در جهت سهولت در کد نویسی وب سایت و ساخت پروژه های php در کمترین زمان ممکن</p>\r\n\r\n<p>به کار گیری اصول امنیتی در ساخت پروژه های php و جلوگیری از حملات معروف از جمله csrf و..</p>\r\n</div>\r\n', '1468095349', '1534139641.png', '1500000', 5, 'file/1.zip', '8.2 گیگابایت', '65 ساعت', 200, 'آموزش,PHP,MVC', 3, 3),
(8, 'پیاده سازی اپلیکیشن اندروید برای وب سایت', 'پیاده-سازی-اپلیکیشن-اندروید-برای-وب-سایت', '<p>به نام خدا</p>\r\n\r\n<p>با عرض سلام و ادب خدمت کاربران</p>\r\n\r\n<p>یکی از راه های موثر برای افزایش ارتباط وب سایت با کاربرانش ساخت نسخه های موبایل برای وب سایت می باشد و البته ساخت نسخه اندروید وب سایت به دلیل فراگیر بودن این سیستم عامل از اهمیت بالایی برخوردار هست از همین جهت در این دوره سعی شده ساخت اپلیکیشن اندروید برای یک وب سایت به صورت کامل و با تمام جزییات آموزش داده شود تا بعد از مشاهده این دوره مخاطبان دوره توانایی ساخت اپلیکیشن اندروید برای انواع وب سایت ها را داشته باشن</p>\r\n', '1468533119', '1534139414.png', '500000', 2, 'file/2.zip', '', '', 0, '', 0, 1),
(9, 'آموزش لاراول', 'آموزش-لاراول', '<p>به نام خدا</p>\r\n\r\n<p>با عرض سلام و ادب خدمت کاربران</p>\r\n\r\n<p>با آموزش PHP لاراول در خدمت شما هستیم</p>\r\n', '1468969363', '1534139248.jpg', '500000', 8, '', '2 گیگابایت', '15 ساعت', 50, '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('59f16114949f475428492a68a9882bb1a674ea5b', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 'YTo4OntzOjY6Il90b2tlbiI7czo0MDoibjY5c3UydnJxdUtDdUhrcUFGWWdLb1hWWWExTFpyUEN5T3JqeXZobCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly9sb2NhbGhvc3Qvc2hvcC9DYXB0Y2hhIjt9czo1OiJmbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjc6IkNhcHRjaGEiO3M6NToiM05TTkwiO3M6NDoiY2FydCI7YToxOntpOjU7aToxO31zOjExOiJ0b3RhbF9wcmljZSI7aToxNTAwMDAwO3M6NToicHJpY2UiO2k6MTUwMDAwMDtzOjk6Il9zZjJfbWV0YSI7YTozOntzOjE6InUiO2k6MTUzNDEzOTY1NjtzOjE6ImMiO2k6MTUzNDEzNjM2OTtzOjE6ImwiO3M6MToiMCI7fX0=', 1534139656);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  `option_value` varchar(255) COLLATE utf8_persian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `option_name`, `option_value`) VALUES
(1, 'TerminalId', ''),
(2, 'UserName', ''),
(3, 'Password', ''),
(4, 'Discounts', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `fname`, `role`, `username`) VALUES
(5, '', 'lashgari@gmail.com', '$2y$10$JEzONtgiQo3lkDTpc6Llv.ZZnXVST4rRN/nF7365juvP9E4TI8.yy', 'xlHR2Q6J18lyq2E0HZd2qh0z0D24f40tbYWBihsYGC2hOH7Hr6JvTCx89OEw', '2018-08-13 00:29:53', '2018-08-13 01:24:16', '', 'admin', 'admin');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
